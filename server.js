const http = require('http');
const app = require('./src/app.js');
const database = require('./src/config/database');

const port = process.env.PORT || 3000;

const server = http.createServer(app);

database().then(info => {
    console.log(`Connected to ${info.host}:${info.port}/${info.name}`);
    server.listen(port, () => {
        console.log('Server on port 3000');
    });
}).catch(() => {
    console.error('Unable to connect to database');
    process.exit(1);
});
