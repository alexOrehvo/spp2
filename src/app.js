const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require("morgan");

app.use('/uploads', express.static('uploads'));
/*  Middlewares  */
app.use(morgan("dev"));
app.use(cors());
app.use(express.urlencoded({extended: false}));
app.use(bodyParser.json());

/*  Routes  */
const taskRoutes = require('./api/routes/todo-list-routing');
const userRoutes = require('./api/routes/user-routing');

app.use('/todo-list', taskRoutes);
app.use('/account', userRoutes);

module.exports = app;
