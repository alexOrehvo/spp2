const mongoose = require('mongoose');

const taskShema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        default: ''
    },
    done: {
        type: Boolean,
        default: false
    },
    startDate: {
        type: Date,
        default: new Date()
    },
    deadline: Date,
    imageUrls: {
        type: [String],
        default: []

    },
    isImportant: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('Task', taskShema);