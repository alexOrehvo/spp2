const multer = require('multer');
const mongoose = require('mongoose');

const dir = './uploads/';
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, dir)
    },
    filename: (req, file, cb) => {
        cb(null, '' + new Date().getTime() + file.originalname)
    }
});
const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
}).single('image');

const Task = require('./../models/task');

const controller = {};

const host = 'http://localhost:3000/uploads/';

controller.getAll = (req, res) => {
    Task.find()
        .select("_id name description done startDate deadline imageUrls isImportant")
        .exec()
        .then(docs => {
            const responce = {
                tasks: docs.map(doc => {
                    return {
                        id: doc._id,
                        name: doc.name,
                        done: doc.done,
                        description: doc.description,
                        startDate: new Date(doc.startDate),
                        deadline: new Date(doc.deadline),
                        imageUrls: doc.imageUrls,
                        isImportant: doc.isImportant
                    }
                })
            };
            res.status(200).json(responce.tasks);
        }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
};

controller.getOneById = (req, res) => {
    const id = req.params.id;
    Task.findById(id)
        .select("_id name description done startDate deadline imageUrls isImportant")
        .exec()
        .then(doc => {
            const task = {
                id: doc._id,
                name: doc.name,
                done: doc.done,
                description: doc.description,
                startDate: doc.startDate,
                deadline: doc.deadline,
                imageUrls: doc.imageUrls,
                isImportant: doc.isImportant
            };
            res.status(200).json(task);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
};

controller.save = (req, res) => {
    const input = JSON.parse(JSON.stringify(req.body));

    const task = new Task({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name
    });

    task.save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: "Create task successfully",
                createdTask: {
                    id: result._id,
                    name: result.name,
                    description: result.description,
                    done: result.done,
                    startDate: result.startDate,
                    deadline: result.deadline,
                    imageUrls: result. imageUrls,
                    isImportant: result.isImportant
                }
            });
        }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
};

controller.delete = (req, res) => {
    const id = req.params.id;

    Task.remove({_id: id})
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Task deleted'
            })
        }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
};

controller.update = (req, res) => {
    const id = req.body.id;
    Task.update({ _id: id }, { $set: req.body })
        .exec()
        .then(result => {
            Task.findById(id)
                .select("_id name description done startDate deadline imageUrls isImportant")
                .exec()
                .then(doc => {
                    const task = {
                        id: doc._id,
                        name: doc.name,
                        done: doc.done,
                        description: doc.description,
                        startDate: doc.startDate,
                        deadline: doc.deadline,
                        imageUrls: doc.imageUrls,
                        isImportant: doc.isImportant
                    };
                    res.status(200).json(task);
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).json({ error: err });
                });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

controller.upload = (req, res)=> {
    var path = '';
    var id = 0;
    upload(req, res, function (err) {
        if (err) {
            // An error occurred when uploading
            console.log(err);
            return res.status(422).send("an Error occured")
        }
        id = req.params.id;
        path = req.file.path;

        Task.updateOne({_id: id}, {$push: {imageUrls: path}}).exec().then(result => {
            req.params.id = id;
            controller.getOneById(req, res);
        }).catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
    });
};

controller.deleteFile = (req, res) => {
    console.log("delete =--------------")
};
module.exports = controller;