const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('./../models/user');

const controller = {};

controller.signUp = (req, res, next) => {
    const email = req.body.email;

    User.findOne({email: email})
        .exec()
        .then(user => {
            if (user) {
                res.status(409).json({
                    message: 'This email already exists'
                });
            } else {
                bcrypt.hash(req.body.password, 10, (err, hash)=> {
                    if (err) {
                        return res.status(500).json({
                            error: err
                        });
                    } else {
                        const user = new User({
                            _id: new mongoose.Types.ObjectId(),
                            email: req.body.email,
                            password: hash
                        });

                        user.save()
                            .then(result => {
                                console.log(result);
                                res.status(201).json({
                                    message: 'User created'
                                });
                            })
                            .catch(err => {
                                if (err) {
                                    return res.status(500).json({
                                        error: err
                                    });
                                }
                            });
                    }
                });
            }
        });
};

controller.login = (req, res, next) => {
    const email = req.body.email;
    console.log(111);
    User.findOne({email: email})
        .exec()
        .then(user => {
            if (user) {
                bcrypt.compare(req.body.password, user.password, (err, result) => {
                    if (err) {
                        console.log(123);
                        res.status(401).json({
                            message: 'Auth failed'
                        });
                    }
                    if (result) {
                        console.log(222);
                        const token = getToken(user);
                        res.status(200).json({
                            message: 'Auth successful',
                            token: token
                        });

                    }
                });
            } else {
                console.log(888);
                res.status(201).json({
                    message: 'Email doesn\'t exists'
                });
            }
        })
        .catch()
};

controller.delete = (req, res, next) => {
    User.remove({_id: req.params.userId})
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'User deleted'
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
};

function getToken(user) {
    const token = jwt.sign({
        email: user.email,
        userId: user._id
    },
        process.env.JWT_KEY,
        {
            expiresIn: "1h"
        });
    return token;
}

module.exports =  controller;
