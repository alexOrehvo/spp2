const express = require('express');
const router = express.Router();
const cors = require('cors');

const userController = require('./../../controller/userController');

router.post('/signup', cors(), userController.signUp);

router.post('/login', cors(), userController.login);

router.delete('/delete:userId', cors(), userController.delete);

module.exports = router;