const express = require('express');
const router = express.Router();
const cors = require('cors');
const taskController = require('./../../controller/taskController');

router.get('/', cors(), taskController.getAll);
router.get('/:id', cors(), taskController.getOneById);

router.post('/', cors(), taskController.save);
router.post('/upload/:id/', cors(), taskController.upload);

router.delete('/:id/', cors(), taskController.delete);

router.put('/', cors(), taskController.update);

module.exports = router;